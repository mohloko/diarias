class AddCpfToDrivers < ActiveRecord::Migration[5.1]
  def change
    add_column :drivers, :cpf, :string
  end
end
