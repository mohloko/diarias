class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :name
      t.references :roles, index: true, foreign_key: true
      add_index :users, :email,                unique: true
      t.timestamps
    end
  end
end
    
    
    
    # Initialize first account:
    User.create! do |u|
        u.name      = 'cleber'
        u.email     = 'cleber@cleber.com'
        u.password    = 'cleber'
    end

