class AddColumnsToDrivers < ActiveRecord::Migration[5.1]
  def change
    add_column :drivers, :banco, :string
    add_column :drivers, :agencia, :string
    add_column :drivers, :conta, :string
  end
end
