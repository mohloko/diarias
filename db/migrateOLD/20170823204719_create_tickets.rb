class CreateTickets < ActiveRecord::Migration[5.1]
  def change
    create_table :tickets do |t|
      t.datetime :start_date
      t.datetime :return_date
      t.decimal :rate
      t.decimal :price
      t.text :annotation
      t.string :status
      t.integer :department_id
      t.integer :origin_id
      t.integer :destination_id
      t.integer :driver_id

      t.timestamps
    end
  end
end
