class AddOriginCityIdToTickets < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :origin_city_id, :integer
  end
end
