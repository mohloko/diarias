class ChangeTypesInTicket < ActiveRecord::Migration[5.1]
  def up
    change_column :tickets, :origin_id, :string
    change_column :tickets, :origin_city_id, :string
    change_column :tickets, :destination_id, :string
    change_column :tickets, :destination_city_id, :string
  end
  
  def down
    change_column :tickets, :origin_id, :integer
    change_column :tickets, :origin_city_id, :integer
    change_column :tickets, :destination_id, :integer
    change_column :tickets, :destination_city_id, :integer
  end
end
