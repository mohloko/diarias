class AddColumnsToTickets < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :manager, :string
    add_column :tickets, :day, :integer
  end
end
