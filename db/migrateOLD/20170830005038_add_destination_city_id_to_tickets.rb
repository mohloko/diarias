class AddDestinationCityIdToTickets < ActiveRecord::Migration[5.1]
  def change
    add_column :tickets, :destination_city_id, :string
  end
end
