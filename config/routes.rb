Rails.application.routes.draw do
  get 'sessions/new'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  #devise_for :users
  #match '/users',   to: 'users#index',   via: 'get'
  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  resources :users
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'


  
  
 
  #resources :users, only: [:index, :show, :edit, :update, :destroy]

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :tickets
  
  resources :drivers
  
  resources :departments
  root to: "tickets#index"
end
