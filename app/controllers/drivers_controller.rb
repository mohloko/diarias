class DriversController < ApplicationController
  
  def index
    @drivers = Driver.all
  end
  
  def show
    @driver = Driver.find(params[:id])
    
  end
  
  def new
    @driver = Driver.new
    
  end
  
  def create
    @driver = Driver.new(driver_params)
    if @driver.save
      flash[:success] = "Motorista cadastrado com sucesso!"
      redirect_to @driver
    else
      flash[:error] = @driver.errors.to_a
      render 'new'
    end
  end
  
  def edit 
    @driver = Driver.find(params[:id])
  end
  
  def update
    @driver = Driver.find(params[:id])
    if @driver.update_attributes(driver_params)
      flash[:success] = "Motorista atualizado!"
      redirect_to @driver
    else
      render 'edit'
    end
  end
  
  def destroy
    Driver.find(params[:id]).destroy
    flash[:success] = "Motorista excluído!"
    redirect_to users_url
  end
  
  private
  
  def driver_params
    params.require(:driver).permit(:name, :banco, :agencia, :conta, :operation, :cpf)
  end
  
end
