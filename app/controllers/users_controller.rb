class UsersController < ApplicationController
  before_action :logged_in_user#, only: [:edit, :update]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: [:destroy]

  def index
    @users = User.all.without_deleted
  end
  
  def show
    @user = User.find(params[:id])
    
  end


  def edit
    @user = User.find(params[:id])
    #authorize! :edit, @user
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Usuario Atualizado"
      redirect_to @user

    else
      render 'edit'
    end
  end


  def new
    @user = User.new
    
  end


  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to @user
    else
      render 'new'
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    if @user.destroy
        redirect_to root_url, notice: "Usuário excluído."
    end
  end
  
  private
  
    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      if current_user.role_id == 4 || current_user?(@user)
      else
        
      #redirect_to(root_url) unless @user == current_user #usa funcao do sessions helper
      redirect_to(root_url) unless current_user?(@user)
end
    end

    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation, :department_id, :role_id)
    end
  end
  

