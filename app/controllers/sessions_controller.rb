class SessionsController < ApplicationController
  def new
  end
  
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user # Log the user in and redirect to the user's show page.
      redirect_to tickets_path
    else
      flash.now[:danger] = 'Conbinação de usuário/senha inválido'
      render 'new'
    end
  end

  
  def destroy
    log_out
    redirect_to root_url
  end



end
