class DepartmentsController < ApplicationController
  def index
    @departments = Department.all
  end
  
  def show
    @department = Department.find(params[:id])
    
  end
  
  def new
    @department = Department.new
  end
  
  def create
    @department = Department.new(department_params)

    if @department.save
      
      flash[:success] = "Unidade cadastrada com sucesso!"
      redirect_to @department
    else
      render 'new'
    end
  end
  
  def edit 
    @department = Department.find(params[:id])
  end
  
  def update
    @department = Department.find(params[:id])
    if @department.update_attributes(department_params)
      flash[:success] = "Unidade atualizada!"
      redirect_to @department
    else
      render 'edit'
    end
  end
  
  def destroy
    Department.find(params[:id]).destroy
    flash[:success] = "Grupo deletado!"
    redirect_to departments_url
  end
  
  private
  
  def department_params
    params.require(:department).permit(:name)
  end

end
