class TicketsController < ApplicationController
  #before_action :logged_in_user, only: [:create, :destroy, :index]
  #load_and_authorize_resource
  before_action :logged_in_user
  before_action :correct_user, only: :destroy
  before_action :admin_user, only: [:edit, :update]
  layout "print", only: :show

  def index
    @search = Ticket.ransack(params[:q])
    #@tickets = Ticket.all.without_deleted
    @tickets = @search.result.without_deleted
    respond_to do |format|
      format.html
      format.csv { send_data @tickets.to_csv }
      format.xls # { send_data @tickets.to_csv(col_sep: "\t") }
    end
    #@search.build_condition
    #@people = @q.result.includes(:articles).page(params[:page])
    
    
  end

  def show
    @ticket = Ticket.find(params[:id])
    #@total = totalprice
    
  end
    
  def edit 
    @ticket = Ticket.find(params[:id])
    #authorize! :edit, @ticket
    
  end
  
  def update
    @ticket = Ticket.find(params[:id])
    if @ticket.update_attributes(ticket_params)
      redirect_to @ticket
      return
    else
      render 'edit'
      return
    end      
  end

  def new
    @ticket = Ticket.new
    @user = current_user
  
    
  end
  
  def create
    @ticket = Ticket.new(ticket_params)
    @ticket.user = current_user
    
    if @ticket.save
      #CommentMailer.reply_ticket(@ticket).deliver_now
      redirect_to tickets_path
    else
      render :new
    end
  end


  
  
  

  def destroy
    @ticket = Ticket.find(params[:id])
    @ticket.destroy
    if @ticket.destroy
      flash[:error] = "Requisição Apagada!"
      redirect_to @ticket
#      redirect_to root_url, notice: "Requisição deletada!"
    end
  end



  private
  
    # Confirms the correct user.

  
  def correct_user
  @ticket = current_user.tickets.find_by(id: params[:id])
  if current_user.role_id == 4 || current_user?(@ticket)
  else
  redirect_to root_url if @ticket.nil?
end
end

  

  def ticket_params
    #params.require(:ticket).permit(:subject, :body, :status, :department_id, :user_id, :type_id, comments_attributes: [:content, :user_id])
    params.require(:ticket).permit(:start_date, :return_date, :rate, :price, :annotation, :status, :department_id,
      :origin_id, :origin_city_id, :driver_id, :destination_id, :destination_city_id)
  end
  
end
