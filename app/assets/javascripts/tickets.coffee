# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on 'turbolinks:load', ->
  jQuery ->
    cities = undefined
    cities = $('#ticket_origin_city_id').html()
    $('#ticket_origin_id').change ->
      options = undefined
      states = undefined
      states = $('#ticket_origin_id :selected').text()
      options = $(cities).filter('optgroup[label=\'' + states + '\']').html()
      if options
        $('#ticket_origin_city_id').html options
      else
        $('#ticket_origin_city_id').empty()
$(document).on 'turbolinks:load', ->
  jQuery ->
    states = undefined
    states = $('#ticket_destination_city_id').html()
    $('#ticket_destination_id').change ->
      country = undefined
      options = undefined
      country = $('#ticket_destination_id :selected').text()
      options = $(states).filter('optgroup[label=\'' + country + '\']').html()
      if options
        $('#ticket_destination_city_id').html options
      else
        $('#ticket_destination_city_id').empty()
  return
$(document).on 'turbolinks:load', ->
  jQuery ->
    $('#mytable').dataTable 'language': 'url': '////cdn.datatables.net/plug-ins/1.10.16/i18n/Portuguese-Brasil.json'