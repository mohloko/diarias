class MyValidator < ActiveModel::Validator
  def validate(record)
    unless record.cpf.starts_with? 'X'
      record.errors[:cpf] << 'CPF Inválido-----'
    end
  end
end
 
#class Driver
 # include ActiveModel::Validations
#  validates_with MyValidator
#end


  