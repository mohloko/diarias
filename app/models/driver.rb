# == Schema Information
#
# Table name: drivers
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  banco      :string
#  agencia    :string
#  conta      :string
#  operation  :string
#  cpf        :string
#

class Driver < ApplicationRecord
  require 'cpf_cnpj'
  validate :checkcpf

  private
  
  def checkcpf
    if !CPF.valid?(self.cpf)
      errors.add(:base, "CPF inválido")
    end
  end
  

end
