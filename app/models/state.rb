# == Schema Information
#
# Table name: states
#
#  id         :integer          not null, primary key
#  symbol     :string
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class State < ApplicationRecord
  has_many :cities
  
  def state_params
    params.require(:state).permit(:name, :symbol)
  end
end
