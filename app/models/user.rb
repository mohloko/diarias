# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  role_id                :integer
#  admin_role             :boolean
#  department_id          :integer
#  password_digest        :string
#  name                   :string
#  deleted_at             :datetime
#

class User < ApplicationRecord
  acts_as_paranoid without_default_scope: true
  belongs_to :department, optional: true
  accepts_nested_attributes_for :department
  belongs_to :role, optional: true
  accepts_nested_attributes_for :role
  has_many :tickets
  
  before_save { self.email = email.downcase }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
      format: { with: VALID_EMAIL_REGEX },
      uniqueness: { case_sensitive: false }
  
  has_secure_password
  
  def self.useradmin
    if current_user.role_id == 4
      has_secure_password validations: false
      validates :password, :password_confirmation, :presence => true, :if => :password
    else
    end
  end
  
  #before_create :set_default_role
  
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable 
  #devise :database_authenticatable, :registerable,
  #:recoverable, :rememberable, :trackable, :validatable
         
  #private
  #def set_default_role
    #self.role ||= Role.find_by_name('registered')
  #end       
  

  protected

  

end
