# == Schema Information
#
# Table name: tickets
#
#  id                  :integer          not null, primary key
#  start_date          :datetime
#  return_date         :datetime
#  rate                :decimal(, )
#  price               :decimal(, )
#  annotation          :text
#  status              :string
#  department_id       :integer
#  origin_id           :string
#  destination_id      :string
#  driver_id           :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  origin_city_id      :string
#  destination_city_id :string
#  manager             :string
#  day                 :integer
#  user_id             :integer
#  deleted_at          :datetime
#

class Ticket < ApplicationRecord
  
  acts_as_paranoid without_default_scope: true
  belongs_to :driver, optional: true
  belongs_to :department, optional: true
  belongs_to :state, optional: true
  belongs_to :city, optional: true
  belongs_to :user, optional: true
  accepts_nested_attributes_for :user
  
  validates_presence_of :driver_id
  
  before_save { self.price.to_s.gsub(',','.').to_f }

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |ticket|
        csv << ticket.attributes.values_at(*column_names)
      end
    end
  end


end
