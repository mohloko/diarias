<% flash.each do |key, value| %>
  <div class="alert alert-<%= key %>">
    <a href="#" data-dismiss="alert" class="close">×</a>
    
      <% type = f[0].to_s.gsub('alert', 'error').gsub('notice', 'alert') %>
        toastr['<%= value %>']('<%= f[1] %>');
      
    
  </div>
<% end %>
